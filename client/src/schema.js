export const typeDefs = `

type Channel {
    id: ID!
    name: String
}

# This type specifies the entry points into Our API. In this case there is
# is only one - "channels" - which returns a list of channels. 
type Query {
    channels: [Channel] # "[]" means this is a list of channels
}
`;