import React, { Component } from 'react';
import './App.css';
import ChannelsListWithData from './componenets/ChannelsListWithData';

import ApolloClient from 'apollo-client';
import { graphql, ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { HttpLink } from 'apollo-link-http';

import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
import { InMemoryCache } from 'apollo-client-preset'
import { typeDefs } from './schema';
import { SchemaLink } from 'apollo-link-schema';      

const schema = makeExecutableSchema({ typeDefs });
addMockFunctionsToSchema({ schema });


const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://localhost:4000/graphql'}),
  cache: new InMemoryCache()
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <div className="navbar">React + GraphQL Tutorial</div>
          <ChannelsListWithData />
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
